package com.xiaochen.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cnmgb
 * @version 1.0
 * @date 2022/8/19
 **/
@RestController
public class HelloController {

  @RequestMapping("/hello")
  public String hello() {
    return "hello";
  }
}
