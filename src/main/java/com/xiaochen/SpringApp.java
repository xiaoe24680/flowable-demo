package com.xiaochen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cnmgb
 * @version 1.0
 * @date 2022/8/19
 **/
@SpringBootApplication
public class SpringApp {

  public static void main(String[] args) {
    SpringApplication.run(SpringApp.class, args);
  }
}
